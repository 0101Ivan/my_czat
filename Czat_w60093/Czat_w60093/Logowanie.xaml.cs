﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Data;

namespace Czat_w60093
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void ZAL_BUT(object sender, EventArgs e)
        {
         

            await Navigation.PushAsync(new Pers_stron());
     
        }

        private async void Nie_mam_kont(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new registr());
        }
    }
}
