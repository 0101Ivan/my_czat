﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Czat_w60093
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class registr : ContentPage
    {
        

        public registr()
        {
            InitializeComponent();
        }

        private async void Zalog_butt_reg(object sender, EventArgs e)
        {
            

            if (email_reg.Text == null)
            {
             await DisplayAlert("Registration","Plese enter your email","ok");
                
            }
             else if (Nazwis_reg.Text == null)
            {
                await DisplayAlert("Registration", "Plese enter your name", "ok");
               
            }
            else if (pass_reg.Text == null)
            {
                await DisplayAlert("Registration", "Plese enter your pass", "ok");
               
            }

            else if (pass_2_reg.Text == null)
            {
                await DisplayAlert("Registration", "Plese enter your second pass", "ok");
                
            }
            else if (pass_reg.Text != pass_2_reg.Text)
            {

                await DisplayAlert("Registration", "pass1 != pass2", "ok");
              
            }
            else
            {

                await Navigation.PushAsync(new Pers_stron());
            }
          
        }
    }
}